﻿using bbtanReplica_Csharp.Albertini_Riccardo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Pasolini_Gianluca.Player
{
    public interface IPlayer
    {
        int Money { get; }
        int BestScore { get; }
        int LastScore { get; }
        string Name { get; set; }

        void AddMoney(int money);

        void UseMoney(int money);

        void SetNewScore(int newScore);

        void ModifyCurrentBallRadius(int radius);

        ISet<BallColor> GetColors();

        void AddNewColor(BallColor newColor);

        BallColor GetCurrentColor();

        void SetCurrentColor(BallColor color);

        int GetCurrentBallRadius();

        ISet<int> GetUnlockedRadius();

        void AddUnlockedRadius(int radius);
    }
}
