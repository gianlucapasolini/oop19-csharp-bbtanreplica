﻿using bbtanReplica_Csharp.BrighiAndrea;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Brighi_Andrea
{
    public interface ICounterWithEvents<T> : ICounter<T>, ISource<T> where T : IComparable
    {
    }
}
