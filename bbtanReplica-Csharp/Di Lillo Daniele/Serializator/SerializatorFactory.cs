﻿using bbtanReplica_Csharp.Pasolini_Gianluca.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Di_Lillo_Daniele.Serializator
{
    public class SerializatorFactory
    {

        public ISerializator<IPlayer> PlayerSerializator()
        {
            return new PlayerSerializator();
        }

        public ISerializator<IPlayersCollection> PlayersCollectionSerializator()
        {
            return new PlayersCollectionSerializator();
        }

        public ISerializator<IPlayersCollection> SecurePlayersCollectionSerializator()
        {
            return new SecurePlayersSerializator(new PlayersCollectionSerializator());
        }
    }
}
