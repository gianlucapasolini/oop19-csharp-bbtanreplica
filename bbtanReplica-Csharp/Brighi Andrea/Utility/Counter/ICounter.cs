﻿using System;
namespace bbtanReplica_Csharp.BrighiAndrea
{
    public interface ICounter<T>
    {
        T Value { get; }
        T Step { get; }
        T Start { get; }

        void Increment();

        void Reset();
    }
}
