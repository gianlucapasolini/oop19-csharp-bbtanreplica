﻿namespace bbtanReplica_Csharp.BrighiAndrea
{
    public class DoubleCounterFactory : AbstractCounterFactory<double>
    {
        private const double BASIC_STEP = 0.1;
        private const double BASIC_START = 0;

        private class DoubleCounter : AbstractCounter
        {

            public DoubleCounter(double startValue, double step) : base(startValue, step, (value, steps) => value + steps)
            {
            }
        }

        public override ICounter<double> CounterFromValueWithStep(double startValue, double step)
        {
            return new DoubleCounter(startValue, step);
        }

        public override ICounter<double> CounterWithStep(double step)
        {
            return CounterFromValueWithStep(BASIC_START, step);
        }

        public override ICounter<double> StandardCounter()
        {
            return CounterFromValueWithStep(BASIC_START, BASIC_STEP);
        }

        public override ICounter<double> StandardCounterFromValue(double startValue)
        {
            return CounterFromValueWithStep(startValue, BASIC_STEP);
        }
    }
}
