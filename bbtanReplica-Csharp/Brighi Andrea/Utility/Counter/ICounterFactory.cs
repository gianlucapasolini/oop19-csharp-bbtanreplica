﻿using System;
namespace bbtanReplica_Csharp.BrighiAndrea
{
    public interface ICounterFactory<T>
    {
        ICounter<T> StandardCounter();

        ICounter<T> StandardCounterFromValue(T startValue);

        ICounter<T> CounterWithStep(T step);

        ICounter<T> CounterFromValueWithStep(T startValue, T step);
    }
}
