﻿using System;
using bbtanReplica_Csharp.Pasolini_Gianluca.Element;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OOP_Test.Pasolini_Gianluca
{
    [TestClass]
    public class Test2D
    {
        private static readonly double DOUBLE_PRECISION = 0.1;
        private static readonly double RADIANS_PRECISION = 0.1;
        private static readonly double DEGREES_PRECISION = 4.0;

        [TestMethod]
        public void TestDistancePoint2D()
        {
            Point2D p0 = new Point2D(0, 0);
            Point2D p1 = new Point2D(0, 1);
            Assert.AreEqual(1, p0.Distance(p1), DOUBLE_PRECISION);
        }

        [TestMethod]
        public void TestSubtractionPoint2D()
        {
            Point2D p0 = new Point2D(0, 1);
            Point2D p1 = new Point2D(1, 2);
            Assert.AreEqual(new Vector2D(1, 1), p1 - p0);
            Assert.AreEqual(new Vector2D(-1, -1), p0 - p1);
        }

        [TestMethod]
        public void TestSumVector2D()
        {
            Point2D p = new Point2D(0, 1);
            Vector2D v = new Vector2D(1, 0);
            Assert.AreEqual(1.0, v.GetModulus(), DOUBLE_PRECISION);
            Assert.AreEqual(0, v.GetRadiansAngle(), RADIANS_PRECISION);
            Assert.AreEqual(new Point2D(1, 1), p + v);
        }

        [TestMethod]
        public void TestNormalizedVector2D()
        {
            double result = 5.0;
            Vector2D v = new Vector2D(3, 4);
            Assert.AreEqual(new Vector2D(3 / result, 4 / result), v.GetNormalizedVector());
            Assert.AreEqual(result, v.GetModulus(), DOUBLE_PRECISION);
        }

        [TestMethod]
        public void TestAngleVector2D()
        {
            Vector2D v = new Vector2D(2, 1);
            Assert.AreEqual(Math.Sqrt(5.0), v.GetModulus(), DOUBLE_PRECISION);
            Assert.AreEqual(Math.PI / 6, v.GetRadiansAngle(), RADIANS_PRECISION);
            Assert.AreEqual(30, v.GetDegreesAngle(), 4);

            v = new Vector2D(-2, 1);
            Assert.AreEqual(Math.Sqrt(5.0), v.GetModulus(), DOUBLE_PRECISION);
            Assert.AreEqual(5 * Math.PI / 6, v.GetRadiansAngle(), RADIANS_PRECISION);
            Assert.AreEqual(150, v.GetDegreesAngle(), DEGREES_PRECISION);

            v = new Vector2D(-2, -1);
            Assert.AreEqual(Math.Sqrt(5.0), v.GetModulus(), DOUBLE_PRECISION);
            Assert.AreEqual(-5 * Math.PI / 6, v.GetRadiansAngle(), RADIANS_PRECISION);
            Assert.AreEqual(-150, v.GetDegreesAngle(), DEGREES_PRECISION);

            v = new Vector2D(2, -1);
            Assert.AreEqual(Math.Sqrt(5.0), v.GetModulus(), DOUBLE_PRECISION);
            Assert.AreEqual(-Math.PI / 6, v.GetRadiansAngle(), RADIANS_PRECISION);
            Assert.AreEqual(-30, v.GetDegreesAngle(), DEGREES_PRECISION);

            v = new Vector2D(-1, 0);
            Assert.AreEqual(Math.Sqrt(1.0), v.GetModulus(), DOUBLE_PRECISION);
            Assert.AreEqual(Math.PI, v.GetRadiansAngle(), RADIANS_PRECISION);
            Assert.AreEqual(180, v.GetDegreesAngle(), DEGREES_PRECISION);
        }

        [TestMethod]
        public void TestNullVector2D()
        {
            Vector2D v = new Vector2D(0, 0);
            Assert.AreEqual(Math.Sqrt(0.0), v.GetModulus(), DOUBLE_PRECISION);
            Assert.IsTrue(v.IsNullVector());
            Assert.ThrowsException<InvalidOperationException>(() => v.GetRadiansAngle());
            Assert.ThrowsException<InvalidOperationException>(() => v.GetDegreesAngle());
            Assert.ThrowsException<InvalidOperationException>(() => v.GetNormalizedVector());
        }
    }
}
