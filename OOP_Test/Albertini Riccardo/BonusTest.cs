﻿using bbtanReplica_Csharp.Albertini_Riccardo.Bonus;
using bbtanReplica_Csharp.Pasolini_Gianluca.Element;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Test.Albertini_Riccardo
{
    [TestClass]
    public class BonusTest
    {
        private static readonly Dictionary<BonusType, Action> dictionary = new Dictionary<BonusType, Action>()
            {
                { BonusType.ADDBALL, () => add = false},
                { BonusType.COIN, () => coin = false},
                { BonusType.HORIZONTAL, () => hor = false},
                { BonusType.VERTICAL, () => ver = false}
            };

        private static bool add = true;
        private static bool coin = true;
        private static bool hor = true;
        private static bool ver = true;

        private readonly BonusFactory factory = new BonusFactory(dictionary);

        [TestMethod]
        public void Testbonustype()
        {
            Point2D pos = new Point2D(2, 4);
            IBonusBlock bAdd = factory.AddBall(pos);
            Assert.IsFalse(bAdd.Hitted);
            Assert.IsTrue(add);
            bAdd.Hit();
            Assert.IsFalse(add);
            Assert.IsTrue(bAdd.Hitted);

            IBonusBlock bCoin = factory.Coin(pos);
            Assert.IsTrue(coin);
            bCoin.Hit();
            Assert.IsFalse(coin);
            Assert.IsTrue(bCoin.Hitted);

            IBonusBlock bHor = factory.Horizontal(pos);
            Assert.IsTrue(hor);
            bHor.Hit();
            Assert.IsFalse(hor);
            Assert.IsTrue(bHor.Hitted);

            IBonusBlock bVer = factory.Vertical(pos);
            Assert.IsTrue(ver);
            bVer.Hit();
            Assert.IsFalse(ver);
            Assert.IsTrue(bVer.Hitted);
        }

        [TestMethod]
        public void TestPosition()
        {
            Point2D pos1 = new Point2D(2, 4);
            Point2D pos2 = new Point2D(3, 5);
            IBonusBlock b = factory.Random(pos1);
            IBonusBlock b2 = factory.Random(pos1);
            Assert.AreEqual(b.Pos, b2.Pos);
            Assert.AreEqual(pos1, b.Pos);
            b.Pos = pos2;
            Assert.AreNotEqual(pos1, b.Pos);
            Assert.AreEqual(pos2, b.Pos);
        }

        [TestMethod]
        public void TestDifferentType()
        {
            Point2D pos = new Point2D(2, 4);
            IBonusBlock bAdd = factory.AddBall(pos);
            IBonusBlock bCoin = factory.Coin(pos);
            Assert.AreNotEqual(bAdd.GetImg(), bCoin.GetImg());
            Assert.AreNotEqual(bAdd, bCoin);
        }
    }
}
