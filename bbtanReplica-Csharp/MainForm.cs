﻿using bbtanReplica_Csharp.Giorgetti_Alex.Blocks;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace bbtanReplica_Csharp
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// This method allow the initial function
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.UserPaint, true);
            this.txtBlock.AppendText("Click on botton to create block line");
        }

        /// <summary>
        /// blockList is a list of all Blocks in the game
        /// countLine is the number of blocklines in the game
        /// y is the Y coordination of blocks
        /// </summary>
        List<BlockViewImpl> blockList = new List<BlockViewImpl>();
        private int countLine = 0;
        private int y = 0;
        private int standardYSpan = 60;

        /// <summary>
        /// This method is used when a new line of blocks is created
        /// </summary>
        private void ChangeCoordination()
        {
            foreach(BlockViewImpl block in this.blockList)
            {
                Rectangle tmpBlock = block.ThisBlock;
                tmpBlock.Y += this.standardYSpan;
                block.ThisBlock = tmpBlock;
                block.updateBrightness();
            }
        }

        /// <summary>
        /// This method create a new blocks line
        /// </summary>
        private void NewBlockLine()
        {
            int amount = new Random().Next(1, 30);

            for (int i = 0; i < 7 * this.standardYSpan; i += this.standardYSpan)
            {
                BlockViewImpl block = new BlockViewImpl(i, y, 49, amount);
                this.blockList.Add(block);
            }

        }

        /// <summary>
        /// This method permit to draw in the form all blocks in blockLine
        /// </summary>
        /// <param name="g"> This variable is used like a draw page</param>
        private void DrawBlock(Graphics g)
        {
            foreach (BlockViewImpl elem in this.blockList)
            {
                g.DrawRectangle(elem.BorderColor, elem.ThisBlock);

                Point textCoordination = elem.setTextCoordination();
                g.DrawString(elem.LogicBlock.ValueBlock.ToString(), new Font("Arial", 12), Brushes.White, textCoordination.X, textCoordination.Y);

            }

            g.Dispose();
        }

        /// <summary>
        /// This is the click method of button btnCreateLine
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateLine_Click(object sender, EventArgs e)
        {
            this.Invalidate();
            this.Refresh();

            countLine += 1;
            if (this.countLine.Equals(7))
            {
                this.btnCreateLine.Enabled = false;
                this.txtBlock.Clear();
                this.txtBlock.AppendText("No more line available!");
            }

            Graphics g = this.CreateGraphics();
            this.ChangeCoordination();
            this.NewBlockLine();
            this.DrawBlock(g);
        }

        /// <summary>
        /// This method allow the user click in the game, and is used for check "block hit"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (BlockViewImpl elem in blockList)
            {
                if ((e.X > elem.ThisBlock.X) && (e.X < elem.ThisBlock.X + elem.ThisBlock.Width) && (e.Y > elem.ThisBlock.Y) && (e.Y < elem.ThisBlock.Y + elem.ThisBlock.Height))
                {
                    this.txtBlock.Clear();
                    this.txtBlock.AppendText("Colpito blocco (" + elem.ThisBlock.X + ", " + elem.ThisBlock.Y + ")");
                    elem.isHitted(1);

                    this.Invalidate();
                    this.Refresh();

                    if (elem.deletedBlock())
                    {
                        this.blockList.Remove(elem);
                    }

                    Graphics g = this.CreateGraphics();
                    this.DrawBlock(g);

                    break;
                }
            }
        }
    }
}
