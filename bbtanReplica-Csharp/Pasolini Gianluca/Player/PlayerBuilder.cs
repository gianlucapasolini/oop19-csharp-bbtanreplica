﻿using bbtanReplica_Csharp.Albertini_Riccardo;
using bbtanReplica_Csharp.Di_Lillo_Daniele;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Pasolini_Gianluca.Player
{
    public class PlayerBuilder : IPlayerBuilder
    {
        private readonly DefaultPlayer player;

        public PlayerBuilder()
        {
            player = new DefaultPlayer();
        }

        public IPlayerBuilder AddNamePlayer(string name)
        {
            player.Name = name.ToUpper();
            return this;
        }

        public IPlayerBuilder AddBestScore(int bestScore)
        {
            player.SetNewScore(bestScore);
            return this;
        }

        public IPlayerBuilder AddInitialMoney(int money)
        {
            player.AddMoney(money);
            return this;
        }

        public IPlayerBuilder AddLastScore(int lastScore)
        {
            player.SetNewScore(lastScore);
            return this;
        }

        public IPlayerBuilder AddUnlockedColors(BallColor color, ISet<BallColor> unlockedColors)
        {
            player.SetColorsList(color, unlockedColors);
            return this;
        }

        public IPlayerBuilder AddUnlockedRadius(int radius, ISet<int> unlockedRadius)
        {
            player.SetRadiusList(radius, unlockedRadius);
            return this;
        }

        public IPlayer Build() => player;
    }
}
