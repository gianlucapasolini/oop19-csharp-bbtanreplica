﻿using System;
namespace bbtanReplica_Csharp.BrighiAndrea
{
    public abstract class AbstractCounterFactory<T> : ICounterFactory<T>
    {
        protected class AbstractCounter : ICounter<T>
        {

            public T Value { get; private set; }

            public T Step { get; private set; }

            public T Start { get; private set; }

            private readonly Func<T, T, T> sum;

            public AbstractCounter(T startValue, T step, Func<T, T, T> sum)
            {
                Start = startValue;
                Step = step;
                Reset();
                this.sum = sum;
            }

            public void Increment()
            {
                Value = sum.Invoke(Value, Step);
            }

            public void Reset()
            {
                Value = Start;
            }
        }

        protected AbstractCounterFactory()
        {
        }

        public abstract ICounter<T> CounterFromValueWithStep(T startValue, T step);
        public abstract ICounter<T> CounterWithStep(T step);
        public abstract ICounter<T> StandardCounter();
        public abstract ICounter<T> StandardCounterFromValue(T startValue);
    }
}
