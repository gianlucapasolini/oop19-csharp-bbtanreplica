﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Text.Json;
using System.Windows.Forms;
using System.IO;
using System.Windows.Forms.VisualStyles;

namespace bbtanReplica_Csharp.Di_Lillo_Daniele.Serializator
{
    public class SerializationMethods
    {
        public static void SerializeSimpleArray<T>(T[] obj, string path, string fileName, string getterKey, string getterValue)
        {
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            if (obj.GetType().IsArray)
            {
                for (int i = 0; i < obj.Length; i++)
                {
                    object o = obj.GetValue(i);
                    PropertyInfo keyMethod = o.GetType().GetProperty(getterKey);
                    PropertyInfo valueMethod = o.GetType().GetProperty(getterValue);

                    dictionary.Add(keyMethod.GetValue(o).ToString(), Convert.ToInt32(valueMethod.GetValue(o)));
                }
                string jsonString = JsonSerializer.Serialize(dictionary);
                if (File.Exists(path + Path.DirectorySeparatorChar + fileName))
                {
                    File.WriteAllText(path + Path.DirectorySeparatorChar + fileName, jsonString);
                }
                else
                {
                    FileStream f = File.Create(path + Path.DirectorySeparatorChar + fileName);
                    f.Close();
                    File.WriteAllText(path + Path.DirectorySeparatorChar + fileName, jsonString);
                }
                
            }
        }

        public static ISet<Int32> DeserializeSimpleArray(JsonElement array)
        {
            ISet<Int32> set = new HashSet<Int32>();
            foreach (JsonElement element in array.EnumerateArray())
            {
                set.Add(element.GetInt32());
            }
            return set;
        }
    }
}
