﻿using bbtanReplica_Csharp.Pasolini_Gianluca.Element;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bbtanReplica_Csharp.Albertini_Riccardo.Bonus
{
    public class BonusFactory : IBonusFactory
    {
        public readonly Dictionary<BonusType, Action> dictionary;

        public BonusFactory(Dictionary<BonusType, Action> dictionary) => this.dictionary = dictionary;

        public IBonusBlock AddBall(Point2D pos) => new BonusBlock(this.dictionary[BonusType.ADDBALL], BonusType.ADDBALL, pos);

        public IBonusBlock Coin(Point2D pos) => new BonusBlock(this.dictionary[BonusType.COIN], BonusType.COIN, pos);

        public IBonusBlock Horizontal(Point2D pos) => new BonusBlock(this.dictionary[BonusType.HORIZONTAL], BonusType.HORIZONTAL, pos);

        public IBonusBlock Vertical(Point2D pos) => new BonusBlock(this.dictionary[BonusType.VERTICAL], BonusType.VERTICAL, pos);

        public IBonusBlock Random(Point2D pos)
        {
            Random random = new Random();
            var enumerable = BonusType.Values;
            BonusType randomType = enumerable.ElementAt(random.Next(0, enumerable.Count()));
            return new BonusBlock(this.dictionary[randomType], randomType, pos);
        }

        private class BonusBlock : IBonusBlock
        {
            private readonly BonusType type;
            private readonly Action action;
            public Point2D Pos { get; set; }
            public bool Hitted { get; private set; }

            internal BonusBlock(Action code, BonusType b, Point2D pos)
            {
                this.type = b;
                this.Pos = pos;
                this.action = code;
            } 

            public string GetImg()
            {
                return this.type.Img;
            }

            public double GetRadius()
            {
                return this.type.Radius;
            }

            public bool IsInstatDelete()
            {
                return this.type.InstaDelete;
            }

            public bool Hit()
            {
                this.action();
                this.Hitted = true;
                return this.IsInstatDelete();
            }
        }
    }
}
