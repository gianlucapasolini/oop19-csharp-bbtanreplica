﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Pasolini_Gianluca.Element
{
    public class Vector2D : IVector2D, IEquatable<Vector2D>
    {
        public double XComponent { get; private set; }
        public double YComponent { get; private set; }

        //constructor.
        public Vector2D(double xComponent, double yComponent)
        {
            XComponent = xComponent;
            YComponent = yComponent;
        }

        private void NullVectorCheck()
        {
            if (IsNullVector())
            {
                throw new InvalidOperationException();
            }
        }

        public double GetRadiansAngle()
        {
            NullVectorCheck();
            return Math.Atan2(YComponent, XComponent);
        }

        public double GetDegreesAngle() => GetRadiansAngle() * (180.0 / Math.PI);

        public bool IsNullVector() => XComponent == 0 && YComponent == 0;

        public double GetModulus()
        {
            return Math.Sqrt(Math.Pow(XComponent, 2) + Math.Pow(YComponent, 2));
        }

        public IVector2D GetNormalizedVector()
        {
            NullVectorCheck();
            double modulus = GetModulus();
            return new Vector2D(XComponent / modulus, YComponent / modulus);
        }

        public bool HasSameNormalizedVector(IVector2D vector2D)
        {
            int precision = -2;
            return HasSameNormalizedVector(vector2D, precision);
        }

        public bool HasSameNormalizedVector(IVector2D vector2D, int precision)
        {
            if (IsNullVector() || vector2D.IsNullVector())
            {
                throw new InvalidOperationException();
            }
            IVector2D v1 = GetNormalizedVector();
            IVector2D v2 = vector2D.GetNormalizedVector();
            double delta = Math.Pow(10, precision);
            return Math.Abs(v1.XComponent - v2.XComponent) <= delta
                    && Math.Abs(v1.YComponent - v2.YComponent) <= delta;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Vector2D);
        }

        public bool Equals(Vector2D other)
        {
            return other != null &&
                   XComponent == other.XComponent &&
                   YComponent == other.YComponent;
        }

        public override int GetHashCode()
        {
            int hashCode = 60958085;
            hashCode = hashCode * -1521134295 + XComponent.GetHashCode();
            hashCode = hashCode * -1521134295 + YComponent.GetHashCode();
            return hashCode;
        }

        public static Vector2D operator *(Vector2D v, double scalar) 
            => new Vector2D(v.XComponent * scalar, v.YComponent * scalar);

        public static Vector2D operator *(Vector2D v1, Vector2D v2)
            => new Vector2D(v1.XComponent * v2.XComponent, v1.YComponent * v2.YComponent);
    }
}
