﻿using bbtanReplica_Csharp.BrighiAndrea;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OOP_Test.Andrea_Brighi
{
    [TestClass]
    public class UtilityTest
    {
        private readonly ICounterFactory<int> intCounterFactory = new IntCounterFactory();

        [TestMethod]
        public void TestCountFrom0()
        {
            int limit = 3;
            ICounter<int> counter = intCounterFactory.StandardCounter();
            Assert.AreEqual(0, counter.Value);
            foreach (int ignored in Enumerable.Range(0, limit))
            {
                counter.Increment();
            }
            Assert.AreEqual(3, counter.Value);
            counter.Reset();
            Assert.AreEqual(0, counter.Value);
        }

        [TestMethod]
        public void TestCountFromN()
        {
            int start = 3;
            int iteration = 3;
            ICounter<int> counter = intCounterFactory.StandardCounterFromValue(start);
            Assert.AreEqual(start, counter.Value);
            foreach (int ignored in Enumerable.Range(0, iteration))
            {
                counter.Increment();
            }
            Assert.AreEqual(start + iteration, counter.Value);
            counter.Reset();
            Assert.AreEqual(start, counter.Value);
        }

        [TestMethod]
        public void TestCountLimit()
        {
            int start = 3;
            int iteration = 3;
            int max = 5;
            ILimitCounterFactory limitCounterFactory = new LimitCounterFactory();
            ICounter<int> counter = limitCounterFactory.LimitCounter(intCounterFactory.StandardCounterFromValue(start), max);
            Assert.AreEqual(start, counter.Value);
            foreach (int ignored in Enumerable.Range(0, iteration))
            {
                counter.Increment();
            }
            Assert.AreEqual(max, counter.Value);
            counter.Reset();
            Assert.AreEqual(start, counter.Value);
        }

        [TestMethod]
        public void TestCountLimitReset()
        {
            int start = 3;
            int iteration = 3;
            int max = 5;
            LimitCounterFactory limitCounterFactory = new LimitCounterFactory();
            ICounter<int> counter = limitCounterFactory.LimitCounterNotifyAndReset(intCounterFactory.StandardCounterFromValue(start), max,
                   (source, value) => Assert.AreEqual(max, value));
            Assert.AreEqual(start, counter.Value);
            foreach (int ignored in Enumerable.Range(0, iteration))
            {
                counter.Increment();
            }
            Assert.AreEqual(max, counter.Value);
        }

        [TestMethod]
        public void TestCountLimitException()
        {
            int start = 3;
            int iteration = 3;
            int max = 5;
            int step = -1;
            LimitCounterFactory limitCounterFactory = new LimitCounterFactory();
            Assert.ThrowsException<ArgumentException>(() => limitCounterFactory.LimitCounter(intCounterFactory.CounterFromValueWithStep(start, step), max));

            ICounter<int> counter = intCounterFactory.CounterFromValueWithStep(start + iteration, step);
            foreach (int ignored in Enumerable.Range(0, iteration))
            {
                counter.Increment();
            }
            Assert.ThrowsException<ArgumentException>(() => limitCounterFactory.LimitCounter(counter, max));
        }

        [TestMethod]
        public void TestObserverAndSource()
        {
            ISource<int> s1 = new Source<int>();
            ISource<int> s2 = new Source<int>();
            int integer = 2;

            void observer(ISource<int> source, int argument)
            {
                Assert.AreEqual(s2, source);
                Assert.AreEqual(integer, argument);
            }

            s1.AddObserver((source, arg) =>
            {
                Assert.AreEqual(s1, source);
                Assert.AreEqual(integer, arg);
            });

            s1.NotifyObservers(integer);

            s2.AddObserver(observer);

            Assert.AreEqual(1, s2.GetObserversSet().Count());
            Assert.AreEqual(true, s2.GetObserversSet().Contains(observer));

            s2.RemoveObserver(observer);

            Assert.AreEqual(0, s2.GetObserversSet().Count());
        }
    }
}
