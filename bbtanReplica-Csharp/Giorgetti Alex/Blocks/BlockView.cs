﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Giorgetti_Alex.Blocks
{
    /// <summary>
    /// This is an interface of BlockView class
    /// </summary>
    public interface BlockView
    {
        /// <summary>
        /// This method allow Get and Set of 
        /// thisBlock variable
        /// </summary>
        Rectangle ThisBlock { get; set; }

        /// <summary>
        /// This method allow Get and Set of 
        /// borderColor variable
        /// </summary>
        Pen BorderColor { get; set; }

        /// <summary>
        /// This method allow Get and Set of 
        /// logicBlock variable
        /// </summary>
        BlockImpl LogicBlock { get; set; }

        /// <summary>
        /// This method set the coordination
        /// X and Y of block Text
        /// Return Point (X, Y)
        /// </summary>
        Point setTextCoordination();

        /// <summary>
        /// This method allow the possibily to change brightness color of block
        /// </summary>
        void updateBrightness();

        /// <summary>
        /// This method used every time one block hitted by balls
        /// </summary>
        /// <param name="value">Amount of block hit</param>
        void isHitted(int value);

        /// <summary>
        /// This method check if block is dead (your life <= 0)
        /// </summary>
        /// <returns>True if is dead, false instead</returns>
        bool deletedBlock();
    }
}
