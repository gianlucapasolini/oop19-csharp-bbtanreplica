﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Di_Lillo_Daniele
{
    class UnlockedItems<T> : IUnlockedItems<T>
    {

        public T CurrentItem { get; private set; }

        public ISet<T> UnlockedSet { get; private set; }

        public UnlockedItems(T currentElement){
            this.UnlockedSet = new HashSet<T>();
            UnlockedSet.Add(currentElement);
            this.CurrentItem = currentElement;
        }

        public UnlockedItems(T currentElement, ISet<T> unlockedSet)
        {
            this.UnlockedSet = unlockedSet;
            this.CurrentItem = currentElement;
            if (!UnlockedSet.Contains(currentElement))
            {
                this.UnlockedSet.Add(currentElement);
            }
        }

        public void AddItem(T newElement)
        {
            this.UnlockedSet.Add(newElement);
            this.CurrentItem = newElement;
        }

        public bool CheckItem(T item)
        {
            return this.UnlockedSet.Contains(item);
        }

        public ISet<T> GetItems()
        {
            return this.UnlockedSet;
        }

        public void SetCurrentItem(T item)
        {
            if (this.CheckItem(item))
            {
                this.CurrentItem = item;
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
}
