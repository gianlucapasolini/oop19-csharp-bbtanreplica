﻿using bbtanReplica_Csharp.Pasolini_Gianluca.Player;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bbtanReplica_Csharp.Di_Lillo_Daniele.Serializator
{
    public class SecurePlayersSerializator : SerializatorDecorator
    {
        public SecurePlayersSerializator(PlayersCollectionSerializator serializator) : base(serializator)
        {
            
        }

        public override void Serialize(string path, string fileName, IPlayersCollection obj)
        {
            Serializator.Serialize(path, fileName, obj);
            var array = (from p in obj.Players
                         select new KeyValuePair<string, int>(p.Name, GetSecureValue(p))).ToArray();
            SerializationMethods.SerializeSimpleArray(array, path, "SecureValues.json", "Key", "Value");
        }

        public override IPlayersCollection DeserializeObject(string path, string fileName)
        {
            PlayersCollection secureColl = new PlayersCollection();
            IPlayersCollection coll = Serializator.DeserializeObject(path, fileName);
            string jsonString = File.ReadAllText(path + Path.DirectorySeparatorChar + "secureValues.json");
            using (JsonDocument document = JsonDocument.Parse(jsonString))
            {
                JsonElement root = document.RootElement;
                foreach (IPlayer p in coll.Players)
                {
                    JsonElement secureValue = root.GetProperty(p.Name);
                    if(GetSecureValue(p) == secureValue.GetInt32())
                    {
                        secureColl.AddPlayer(p);
                    }
                    else
                    {
                        Console.WriteLine("Founded one player corrupted");
                    }
                    if (secureColl.ContainsPlayer(coll.CurrentPlayerName.Get()))
                    {
                        secureColl.CurrentPlayerName = coll.CurrentPlayerName;
                    }
                }
            }
            secureColl.Update();
            this.Serialize(path, fileName, secureColl);
            return secureColl;
        }

        private int GetSecureValue(IPlayer player)
        {
            return player.BestScore + player.Name.Length + player.Money;
        }

    }
}
