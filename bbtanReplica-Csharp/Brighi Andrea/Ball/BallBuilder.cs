﻿using System;
using bbtanReplica_Csharp.BrighiAndrea;
using bbtanReplica_Csharp.Pasolini_Gianluca;
using bbtanReplica_Csharp.Pasolini_Gianluca.Element;

namespace bbtanReplica_Csharp.BrighiAndrea.Ball
{
    public class BallBuilder : IBallBuilder
    {
        private const double STANDARD_SPEED_INCREMENT = 5;
        private const double STANDARD_SPEED_LIMIT = 200;
        private const int STANDARD_INCREMENT_SPEED_STEP = 20;

        private double radius = 1;
        private int damage = 1;
        private double startSpeed = 1;
        private double incrementSpeed = STANDARD_SPEED_INCREMENT;
        private double speedLimit = STANDARD_SPEED_LIMIT;
        private int incrementSpeedStep = STANDARD_INCREMENT_SPEED_STEP;
        private Movement movement;
        private Stop stop;
        private Point2D startPosition;
        private bool ballAbleToCollide = true;

        public IBallBuilder AddBallCanCollide()
        {
            ballAbleToCollide = true;
            return this;
        }

        public IBallBuilder AddBallCanNotCollide()
        {
            ballAbleToCollide = false;
            return this;
        }

        public IBallBuilder AddDamage(int damage)
        {
            if (damage <= 0)
            {
                throw new ArgumentException();
            }
            this.damage = damage;
            return this;
        }

        public IBallBuilder AddStartSpeed(double startSpeed)
        {
            if (startSpeed <= 0)
            {
                throw new ArgumentException();
            }
            this.startSpeed = startSpeed;
            return this;
        }


        public IBallBuilder AddSpeedLimit(double speedLimit)
        {
            if (speedLimit < startSpeed)
            {
                throw new ArgumentException();
            }
            this.speedLimit = speedLimit;
            return this;
        }

        public IBallBuilder AddIncrementSpeed(double incrementSpeedPercent)
        {
            if (startSpeed <= 0 && incrementSpeedPercent < 0)
            {
                throw new ArgumentException();
            }
            incrementSpeed = incrementSpeedPercent;
            return this;
        }

        public IBallBuilder AddIncrementSpeedStep(int incrementSpeedStep)
        {
            if (incrementSpeedStep <= 10)
            {
                throw new ArgumentException();
            }
            this.incrementSpeedStep = incrementSpeedStep;
            return this;
        }

        public IBallBuilder AddMovement(Movement movement)
        {
            if (movement is null)
            {
                throw new ArgumentException();
            }
            this.movement = movement;
            return this;
        }

        public IBallBuilder AddRadius(double radius)
        {
            if (radius <= 0)
            {
                throw new ArgumentException();
            }
            this.radius = radius;
            return this;
        }

        public IBallBuilder AddStartPosition(Point2D startPosition)
        {
            if (startPosition is null)
            {
                throw new ArgumentException();
            }
            this.startPosition = startPosition;
            return this;
        }

        public IBallBuilder AddStop(Stop stop)
        {
            if (stop is null)
            {
                throw new ArgumentException();
            }
            this.stop = stop;
            return this;
        }

        public IBall Build()
        {
            if ((startPosition is null) || speedLimit < startSpeed)
            {
                throw new ArgumentException();
            }
            double speedIncrement = startSpeed * incrementSpeed / 100;
            if (stop is null)
            {
                stop = (ball) =>
                {
                };
            }
            if (movement is null)
            {
                movement = (ball) =>
                {
                };
            }
            return new Ball(startSpeed, speedIncrement, speedLimit, incrementSpeedStep)
            {
                Position = startPosition,
                Radius = radius,
                Damage = damage,
                IsAbleToCollide = ballAbleToCollide,
                Movement = movement,
                Stop = stop
            };
        }

        private class Ball : IBall
        {
            private readonly ICounter<double> speed;
            private readonly ICounter<int> step;
            private Vector2D _direction = new Vector2D(0, 0);
            private Optional<Point2D> destination = Optional<Point2D>.Empty();

            public Point2D Position { get; internal set; }
            public double Radius { get; internal set; }
            public int Damage { get; internal set; }
            public bool IsInsideAnotherObject { get; private set; }
            public bool IsAbleToCollide { get; internal set; }
            internal Stop Stop { get; set; }
            internal Movement Movement { get; set; }

            public Ball(double startSpeed, double speedIncrement, double speedLimit, int incrementSpeedStep)
            {
                ILimitCounterFactory limit = new LimitCounterFactory();
                speed = limit.LimitCounter(new DoubleCounterFactory().CounterFromValueWithStep(startSpeed, speedIncrement), speedLimit);
                step = limit.LimitCounterNotifyAndReset(new IntCounterFactory().StandardCounter(), incrementSpeedStep, (source, argument) => IncrementSpeed());
            }

            public double Speed
            {
                get
                {
                    return Direction.IsNullVector() ? 0 : speed.Value;
                }
            }

            public Vector2D Direction
            {
                get
                {
                    return _direction;
                }

                set
                {
                    if (value is null)
                    {
                        throw new ArgumentException();
                    }
                    if (!_direction.Equals(value))
                    {
                        _direction = value;
                        if (_direction.IsNullVector())
                        {
                            ResetSpeed();
                            step.Reset();
                            Stop(this);
                        }
                        else
                        {
                            _direction = (Vector2D)Direction.GetNormalizedVector();
                        }
                    }
                }
            }

            public void AbleToCollide()
            {
                IsAbleToCollide = true;
            }

            public void NotAbleToCollide()
            {
                IsAbleToCollide = false;
            }

            public void IncrementSpeed()
            {
                speed.Increment();
            }

            public void ResetSpeed()
            {
                speed.Reset();
            }

            public void InsideAnotherObject()
            {
                IsInsideAnotherObject = true;
            }

            public void NotInsideAnotherObject()
            {
                IsInsideAnotherObject = false;
            }

            public bool IsStationary
            {
                get
                {
                    return Direction.IsNullVector();
                }
            }

            public void SetDestination(Point2D destination)
            {
                if (Position.Equals(destination))
                {
                    Direction = new Vector2D(0, 0);
                }
                else
                {
                    this.destination = Optional<Point2D>.Of(destination);
                }
            }

            public void MoveByTime(double timeInterval)
            {
                if (!IsStationary && timeInterval != 0)
                {
                    MoveByDistance(timeInterval * Speed);
                    step.Increment();
                }
            }

            public void MoveByDistance(double distance)
            {
                if (destination.IsPresent)
                {
                    ReachDestination(distance);
                }
                Move(Direction * distance);
            }

            private void ReachDestination(double distance)
            {
                if (destination.IsPresent)
                {
                    Vector2D difference = destination.Get() - Position;
                    if (IsDestinationReachable(difference, distance))
                    {
                        Move(difference);
                        Direction = new Vector2D(0, 0);
                        destination = Optional<Point2D>.Empty();
                    }
                }
            }

            private bool IsDestinationReachable(Vector2D difference, double distance)
            {
                return difference.HasSameNormalizedVector(Direction) && difference.GetModulus() <= distance;
            }

            private void Move(Vector2D distance)
            {
                if (!distance.IsNullVector())
                {
                    Position += distance;
                    Movement(this);
                }
            }

            public void Collision(Point2D centerPoint, Vector2D newDirection)
            {
                if ((centerPoint is null) || (newDirection is null))
                {
                    throw new ArgumentException();
                }
                if (IsAbleToCollide && !centerPoint.Equals(Position))
                {
                    Vector2D distance = Position - centerPoint;
                    if (this.IsSameNormalizedVectorNotNull(distance))
                    {
                        throw new ArgumentException();
                    }
                    Position = centerPoint;
                    Direction = newDirection;
                    MoveByDistance(distance.GetModulus());
                }
            }

            private bool IsSameNormalizedVectorNotNull(Vector2D distance)
            {
                return distance.IsNullVector() || Direction.IsNullVector()
                        || !distance.HasSameNormalizedVector(Direction);
            }
        }
    }
}
