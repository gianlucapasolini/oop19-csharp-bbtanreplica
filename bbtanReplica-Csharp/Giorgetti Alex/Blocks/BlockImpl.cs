﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Giorgetti_Alex.Blocks
{
    /// <summary>
    /// This class implements Block class and permit 
    /// to create a logic Block
    /// </summary>
    public class BlockImpl : Block
    {
        public int BlockSize    // the Name property
        { get; set; }

        public int ValueBlock    // the Name property
        { get; set; }

        /// <summary> Class constructor </summary>
        public BlockImpl(int valueBlock, int blockSize)
        {
            this.ValueBlock = valueBlock;
            this.BlockSize = blockSize;
        }
    }
}
