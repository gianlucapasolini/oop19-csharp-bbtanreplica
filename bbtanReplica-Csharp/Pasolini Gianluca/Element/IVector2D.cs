﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Pasolini_Gianluca.Element
{
    public interface IVector2D
    {
        double XComponent { get; }
        double YComponent { get; }

        double GetRadiansAngle();

        double GetDegreesAngle();

        bool IsNullVector();

        double GetModulus();

        IVector2D GetNormalizedVector();

        bool HasSameNormalizedVector(IVector2D vector2D);

        bool HasSameNormalizedVector(IVector2D vector2D, int precision);
    }
}
