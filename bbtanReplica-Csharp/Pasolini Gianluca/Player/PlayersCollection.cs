﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using bbtanReplica_Csharp.Di_Lillo_Daniele.Serializator;

namespace bbtanReplica_Csharp.Pasolini_Gianluca.Player
{
    public class PlayersCollection : IPlayersCollection
    {
        private const string FILENAME = "PlayerSer.json";
        public IList<IPlayer> Players { get; set; }
        private Optional<string> currentPlayerName;
        public Optional<string> CurrentPlayerName 
        {
            get
            {
                return currentPlayerName;
            }
            set
            {
                if (value.IsEmpty() || value.Get() == null)
                    currentPlayerName = Optional<string>.Empty();
                else
                    currentPlayerName = Optional<string>.Of(value.Get().ToUpper());
            }
        }

        public PlayersCollection()
        {
            Players = new List<IPlayer>();
            CurrentPlayerName = Optional<string>.Empty();
        }

        public static IPlayersCollection GetCollectionFromDisk()
        {
            string path = GetPath();
            ISerializator<IPlayersCollection> ser = new SerializatorFactory().SecurePlayersCollectionSerializator();
            Optional<IPlayersCollection> optCollection = Optional<IPlayersCollection>.Of(ser.DeserializeObject(path, FILENAME));
            if (optCollection.IsPresent) {
                return optCollection.Get();
            } else {
                PlayersCollection newCollection = new PlayersCollection();
                ser.Serialize(path, FILENAME, newCollection);
                return newCollection;
            }
        }

        private static string GetPath()
        {
            string path;
            if (Environment.OSVersion.Platform.ToString().ToLower().Contains("win"))
                path = Environment.GetEnvironmentVariable("APPDATA");
            else
                path = Environment.GetEnvironmentVariable("HOME");
            return path += Path.DirectorySeparatorChar + "BBTAN" + Path.DirectorySeparatorChar + "csharp";
        }

        public bool ContainsPlayer(string name)
        {
            if (Players.Count == 0 || name == null)
                return false;
            return Players.Any(p => p.Name == name.ToUpper());
        }

        public void AddPlayer(IPlayer player)
        {
            if (ContainsPlayer(player.Name.ToUpper()))
                throw new InvalidOperationException();
            Players.Add(player);
        }

        public void AddPlayer(string name)
        {
            if (ContainsPlayer(name.ToUpper()))
                throw new InvalidOperationException();
            Players.Add(new PlayerBuilder().AddNamePlayer(name.ToUpper()).Build());
        }

        public Optional<IPlayer> GetBestPlayer()
        {
            if (Players.Count == 0)
                return Optional<IPlayer>.Empty();
            return Optional<IPlayer>.Of(Players.First(p => p.BestScore == Players.Max(b => b.BestScore)));
        }

        public Optional<IPlayer> GetPlayer(string name)
        {
            if (ContainsPlayer(name.ToUpper()))
                return Optional<IPlayer>.Of(Players.First(p => p.Name == name.ToUpper()));
            return Optional<IPlayer>.Empty();
        }

        public void Update()
        {
            string path = GetPath();
            ISerializator<IPlayersCollection> ser = new SerializatorFactory().SecurePlayersCollectionSerializator();
            ser.Serialize(path, FILENAME, this);
        }

        public void Logout()
        {
            CurrentPlayerName = Optional<string>.Empty();
            this.Update();
        }

        public Optional<IList<IPlayer>> GetPlayersInOrderOfBestScore()
        {
            if (Players.Count == 0)
                return Optional<IList<IPlayer>>.Empty();
            return Optional<IList<IPlayer>>.Of(Players.OrderBy(p => p.BestScore).Reverse().ToList());
        }

        public void Reset()
        {
            Players = new List<IPlayer>();
        }

        public override string ToString()
        {
            return "{" + Players.Select(p => p.Name).Aggregate((a, b) => a + "," + b) + "}";
        }
    }
}
