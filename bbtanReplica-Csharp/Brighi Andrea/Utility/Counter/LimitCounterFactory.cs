﻿using System;

namespace bbtanReplica_Csharp.BrighiAndrea
{
    public class LimitCounterFactory : ILimitCounterFactory
    {
        public ICounter<T> LimitCounter<T>(ICounter<T> counter, T limit) where T : IComparable
        {
            return LimitCounterNotify(counter, limit, true);
        }

        public ICounterWithEvents<T> LimitCounterNotifyAndReset<T>(ICounter<T> counter, T limit) where T : IComparable
        {
            return LimitCounterNotify(counter, limit, true);
        }

        public ICounterWithEvents<T> LimitCounterNotifyAndReset<T>(ICounter<T> counter, T limit, IObserver<T> limitObserver) where T : IComparable
        {
            ICounterWithEvents<T> counterWithEvents = LimitCounterNotifyAndReset(counter, limit);
            counterWithEvents.AddObserver(limitObserver);
            return counterWithEvents;
        }

        private ICounterWithEvents<T> LimitCounterNotify<T>(ICounter<T> counter, T limit, bool reset) where T : IComparable
        {
            int initialComparation = limit.CompareTo(counter.Start);
            if (initialComparation == -limit.CompareTo(counter.Value)
                    || initialComparation != Math.Sign(Convert.ToDouble(counter.Step)))
            {
                throw new ArgumentException();
            }
            return new CounterWithEventsImpl<T>(counter, limit, initialComparation, reset);
        }

        private class CounterWithEventsImpl<T> : Source<T>, ICounterWithEvents<T> where T : IComparable
        {
            private readonly ICounter<T> counter;
            private readonly T limit;
            private readonly int initialComparation;
            private readonly bool reset;

            public CounterWithEventsImpl(ICounter<T> counter, T limit, int initialComparation, bool reset)
            {
                this.counter = counter;
                this.limit = limit;
                this.initialComparation = initialComparation;
                this.reset = reset;
            }

            public T Value => counter.Value;

            public T Step => counter.Step;

            public T Start => counter.Start;

            public void Increment()
            {
                if (limit.CompareTo(counter.Value) == initialComparation)
                {
                    counter.Increment();
                    if (limit.CompareTo(counter.Value) == -initialComparation)
                    {
                        NotifyObservers(Value);
                        if (reset)
                        {
                            Reset();
                        }
                    }
                }
            }

            public void Reset()
            {
                counter.Reset();
            }
        }
    }
}
