﻿using bbtanReplica_Csharp.Pasolini_Gianluca.Element;

namespace bbtanReplica_Csharp.BrighiAndrea.Ball
{
    public interface IBall
    {
        double Radius { get; }

        double Speed { get; }

        void IncrementSpeed();

        void ResetSpeed();

        int Damage { get; }

        Point2D Position { get; }

        Vector2D Direction { get; set; }

        bool IsStationary { get; }

        void MoveByTime(double timeInterval);

        void MoveByDistance(double distance);

        void Collision(Point2D centerPoint, Vector2D newDirection);

        void SetDestination(Point2D destination);

        bool IsAbleToCollide { get; }

        void NotAbleToCollide();

        void AbleToCollide();

        bool IsInsideAnotherObject { get; }

        void InsideAnotherObject();

        void NotInsideAnotherObject();
    }
}
