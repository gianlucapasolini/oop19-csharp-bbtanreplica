﻿using System;
using bbtanReplica_Csharp.Pasolini_Gianluca.Element;

namespace bbtanReplica_Csharp.BrighiAndrea.Ball
{

    public delegate void Movement(IBall ball);

    public delegate void Stop(IBall ball);

    public interface IBallBuilder
    {
        IBall Build();

        IBallBuilder AddRadius(double radius);

        IBallBuilder AddDamage(int damage);

        IBallBuilder AddStartSpeed(double startSpeed);

        IBallBuilder AddIncrementSpeed(double incrementSpeedPercent);

        IBallBuilder AddIncrementSpeedStep(int incrementSpeedStep);

        IBallBuilder AddSpeedLimit(double speedLimit);

        IBallBuilder AddStartPosition(Point2D startPosition);

        IBallBuilder AddMovement(Movement movement);

        IBallBuilder AddStop(Stop stop);

        IBallBuilder AddBallCanCollide();

        IBallBuilder AddBallCanNotCollide();
    }
}
