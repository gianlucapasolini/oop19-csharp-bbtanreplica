﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using bbtanReplica_Csharp.Giorgetti_Alex.Blocks;
using System.Drawing;

namespace OOP_Test.Giorgetti_Alex
{
    [TestClass]
    public class BlockTest
    {

        [TestMethod]
        public void CreateSimpleBlock()
        {
            int value = 1000;
            int blockSize = 1000;

            Block testBlock = new BlockImpl(value, blockSize);
            Assert.IsNotNull(testBlock.BlockSize);
            Assert.IsNotNull(testBlock.ValueBlock);
        }

        [TestMethod]
        public void CreateSimpleBlockView()
        {
            BlockView testBlockView = new BlockViewImpl(50, 50, 50, 5); 
            Assert.IsFalse(testBlockView.deletedBlock());
            testBlockView.isHitted(3);
            Assert.AreEqual(2, testBlockView.LogicBlock.ValueBlock);
            testBlockView.isHitted(2);
            Assert.IsTrue(testBlockView.deletedBlock());

            BlockView testBlockView2 = new BlockViewImpl(50, 50, 50, 28);
            Assert.AreEqual(Color.Yellow, testBlockView2.BorderColor.Color);
            testBlockView2.isHitted(15);
            Assert.AreNotEqual(Color.Green, testBlockView2.BorderColor.Color);

            Color newGreen = Color.FromArgb(Color.Green.A,
                (int)(Color.Green.R * 0.85), (int)(Color.Green.G * 0.85), (int)(Color.Green.B * 0.85));

            Assert.AreEqual(newGreen, testBlockView2.BorderColor.Color);
        }

    }
}
