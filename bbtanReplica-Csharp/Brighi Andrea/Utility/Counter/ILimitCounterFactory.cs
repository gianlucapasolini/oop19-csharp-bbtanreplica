﻿using System;

namespace bbtanReplica_Csharp.BrighiAndrea
{
    public interface ILimitCounterFactory
    {
        ICounter<T> LimitCounter<T>(ICounter<T> counter, T limit) where T : IComparable;

        ICounterWithEvents<T> LimitCounterNotifyAndReset<T>(ICounter<T> counter, T limit) where T : IComparable;

        ICounterWithEvents<T> LimitCounterNotifyAndReset<T>(ICounter<T> counter, T limit, IObserver<T> limitObserver) where T : IComparable;
    }
}
