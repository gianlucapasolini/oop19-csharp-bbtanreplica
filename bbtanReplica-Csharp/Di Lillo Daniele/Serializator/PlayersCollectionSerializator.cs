﻿using bbtanReplica_Csharp.Pasolini_Gianluca.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Windows.Forms;
using bbtanReplica_Csharp.Pasolini_Gianluca;

namespace bbtanReplica_Csharp.Di_Lillo_Daniele.Serializator
{
    public class PlayersCollectionSerializator : AbstractSerializator<IPlayersCollection>
    {
        public override string ConvertIntoJson(IPlayersCollection obj)
        {
            PlayerSerializator serializator = new PlayerSerializator();
            string currentPlayer = "";
            if (!obj.CurrentPlayerName.IsEmpty())
            {
                currentPlayer = obj.CurrentPlayerName.Get();
            }
            var serObj = new
            {
                CurrentPlayer = currentPlayer,
                Players = (from p in obj.Players
                           select serializator.ConvertObject(p))
            };
            
            string json = JsonSerializer.Serialize(serObj);
            return json;
        }

        public override IPlayersCollection ConvertIntoObject(string jsonString)
        {
            using (JsonDocument document = JsonDocument.Parse(jsonString))
            {
                JsonElement root = document.RootElement;
                string currentPlayerName = root.GetProperty("CurrentPlayer").GetString();
                List<IPlayer> playersList = new List<IPlayer>();
                JsonElement players = root.GetProperty("Players");
                foreach (JsonElement player in players.EnumerateArray())
                {
                    PlayerSerializator plSer = new PlayerSerializator();
                    IPlayer p = plSer.ConvertIntoObject(player.ToString());
                    playersList.Add(p);
                }
                IPlayersCollection coll = new PlayersCollection();
                Optional<string> opt = Optional<string>.Of(currentPlayerName);
                coll.CurrentPlayerName = opt;
                coll.Players = playersList;
                return coll;
            }
        }
    }
}
