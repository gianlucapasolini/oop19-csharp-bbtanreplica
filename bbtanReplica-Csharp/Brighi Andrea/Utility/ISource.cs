﻿using System.Collections.Generic;

namespace bbtanReplica_Csharp.BrighiAndrea
{
    public interface ISource<T>
    {
        void AddObserver(IObserver<T> observer);

        void RemoveObserver(IObserver<T> observer);

        ISet<IObserver<T>> GetObserversSet();

        void NotifyObservers(T arg);
    }
}
