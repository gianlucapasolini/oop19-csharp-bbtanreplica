﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Windows.Forms;
using System.IO;

namespace bbtanReplica_Csharp.Di_Lillo_Daniele.Serializator
{
    public interface ISerializator<T>
    {

        string ConvertIntoJson(T obj);

        T ConvertIntoObject(string jsonString);

        T DeserializeObject(string path, string fileName);

        void Serialize(string path, string fileName, T obj);
    }
}
