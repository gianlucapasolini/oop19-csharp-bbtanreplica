﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Pasolini_Gianluca.Element
{
    public interface IPoint2D
    {
        double X { get; }
        double Y { get; }

        double Distance(IPoint2D p);
    }
}
