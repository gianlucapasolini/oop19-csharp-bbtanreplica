﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Giorgetti_Alex.Blocks
{
    /// <summary>
    /// This is an Inerface of class Block
    /// </summary>
    public interface Block
    {
        /// <summary>
        /// This method allow Get and Set of 
        /// blockSize variable
        /// </summary>
        int BlockSize { get; set; }

        /// <summary>
        /// This method allow Get and Set of 
        /// valueBlock variable
        /// </summary>
        int ValueBlock { get; set; }
    }
}
