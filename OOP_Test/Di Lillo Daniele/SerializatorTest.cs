﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bbtanReplica_Csharp.Di_Lillo_Daniele.Serializator;
using bbtanReplica_Csharp.Pasolini_Gianluca.Player;
using System.Windows.Forms;
using bbtanReplica_Csharp.Pasolini_Gianluca;
using System.IO;

namespace OOP_Test.Di_Lillo_Daniele
{
    [TestClass]
    public class SerializatorTest
    {
        PlayersCollection coll;
        readonly string nameFile = Path.DirectorySeparatorChar + "PlayerSerTest.json";
        string path = "";

        [TestInitialize]
        public void TestInitialize()
        {
            if (Environment.OSVersion.Platform.ToString().ToLower().Contains("win"))
                path = Environment.GetEnvironmentVariable("APPDATA");
            else
                path = Environment.GetEnvironmentVariable("HOME");
            path += Path.DirectorySeparatorChar + "BBTAN" + Path.DirectorySeparatorChar + "csharp" + Path.DirectorySeparatorChar + "Test";
            coll = new PlayersCollection();
            coll.AddPlayer(new PlayerBuilder().AddNamePlayer("Daniele").AddBestScore(50).AddInitialMoney(5).Build());
            coll.AddPlayer(new PlayerBuilder().AddNamePlayer("Ciccio").AddBestScore(30).AddInitialMoney(3).Build());
            coll.AddPlayer(new PlayerBuilder().AddNamePlayer("Friele").AddBestScore(20).AddInitialMoney(2).Build());
        }

        [TestMethod]
        public void PlayersSerializationTest()
        {
            IPlayer p = new PlayerBuilder().AddNamePlayer("Daniele").AddBestScore(50).AddInitialMoney(5).Build();
            ISerializator<IPlayer> ser = new SerializatorFactory().PlayerSerializator();
            ser.Serialize(path, nameFile, p);
            IPlayer newP = ser.DeserializeObject(path, nameFile);
            Assert.AreEqual(p.Name, newP.Name);
            Assert.AreEqual(p.BestScore, newP.BestScore);
        }

        [TestMethod]
        public void DeSerializationTest()
        {
            Optional<string> opt = Optional<string>.Of("Friele");
            coll.CurrentPlayerName = opt;

            PlayersCollectionSerializator pSer = new PlayersCollectionSerializator();
            pSer.ConvertIntoObject(pSer.ConvertIntoJson(coll));
            pSer.Serialize(path, nameFile, coll);
            IPlayersCollection newColl = pSer.DeserializeObject(path, nameFile);
            Assert.AreEqual(coll.GetBestPlayer().Get().Name.ToString(), newColl.GetBestPlayer().Get().Name.ToString());
            Assert.AreEqual(coll.Players.Count(), newColl.Players.Count());
            Assert.AreEqual(coll.CurrentPlayerName.Get(), newColl.CurrentPlayerName.Get());
        }
        
        [TestMethod]
        public void SecureSerializationTest()
        {
            SerializatorFactory sf = new SerializatorFactory();
            ISerializator<IPlayersCollection> sSer = sf.SecurePlayersCollectionSerializator();            

            sSer.Serialize(path, nameFile, coll);
            IPlayersCollection newColl = sSer.DeserializeObject(path, nameFile);

            Assert.AreEqual(coll.GetBestPlayer().Get().Name.ToString(), newColl.GetBestPlayer().Get().Name.ToString());
            Assert.AreEqual(coll.Players.Count(), newColl.Players.Count());
            Assert.AreEqual(coll.CurrentPlayerName.Get(), newColl.CurrentPlayerName.Get());
        }

        [TestMethod]
        public void SecureSerializationCorruptedTest()
        {
            SerializatorFactory sf = new SerializatorFactory();
            ISerializator<IPlayersCollection> sSer = sf.SecurePlayersCollectionSerializator();

            sSer.Serialize(path, nameFile, coll);
            IPlayersCollection newColl = sSer.DeserializeObject(path, nameFile);

            ISerializator<IPlayersCollection> pSer = sf.PlayersCollectionSerializator();
            IPlayersCollection savedColl = pSer.DeserializeObject(path, nameFile);
            Optional<IPlayer> opt = savedColl.GetBestPlayer();
            if (opt.IsEmpty())
            {
                return;
            }
            savedColl.GetPlayer("Daniele").Get().AddMoney(999);
            pSer.Serialize(path, nameFile, savedColl);

            IPlayersCollection newModifiedColl = sSer.DeserializeObject(path, nameFile);
            Assert.IsFalse(newModifiedColl.ContainsPlayer("Daniele"));
        }
    }
}
