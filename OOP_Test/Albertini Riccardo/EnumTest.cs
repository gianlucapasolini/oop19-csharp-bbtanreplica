﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bbtanReplica_Csharp.Albertini_Riccardo;
using bbtanReplica_Csharp.Albertini_Riccardo.Bonus;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace OOP_Test.Albertini_Riccardo
{
    [TestClass]
    public class EnumTest
    {
        [TestMethod]
        public void TestValuesBonusType()
        {
            var listreturn = BonusType.Values;
            IList<BonusType> list = new List<BonusType>()
            {
                BonusType.COIN,
                BonusType.ADDBALL,
                BonusType.HORIZONTAL,
                BonusType.VERTICAL
            };
            Assert.IsTrue(list.All(listreturn.Contains) && list.Count() == listreturn.Count());
        }

        [TestMethod]
        public void TestValuesColor()
        {
            var listreturn = BallColor.Values;
            List<BallColor> list = new List<BallColor>
            {
                BallColor.AQUA_MARINE,
                BallColor.DARK_PURPLE,
                BallColor.BLUE,
                BallColor.FUCSIA,
                BallColor.GREEN,
                BallColor.LIGHT_BLUE,
                BallColor.LIGHT_GREEN,
                BallColor.ORANGE,
                BallColor.PURPLE,
                BallColor.RED,
                BallColor.VIOLET,
                BallColor.WHITE,
                BallColor.YELLOW
            };

            Assert.IsTrue(list.All(listreturn.Contains) && list.Count() == listreturn.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
        "A wrong index has been passed.")]
        public void TestIndex()
        {
            Assert.AreEqual(BallColor.GetColorByIndex(0), BallColor.RED);
            BallColor.GetColorByIndex(20);
            Assert.AreEqual(BonusType.GetBonusByIndex(0), BonusType.COIN);
            BonusType.GetBonusByIndex(50);
        }
    }
}
