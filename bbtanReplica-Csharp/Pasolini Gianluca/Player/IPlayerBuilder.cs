﻿using bbtanReplica_Csharp.Albertini_Riccardo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Pasolini_Gianluca.Player
{
    public interface IPlayerBuilder
    {
        IPlayerBuilder AddNamePlayer(string name);

        IPlayerBuilder AddBestScore(int bestScore);

        IPlayerBuilder AddInitialMoney(int money);

        IPlayerBuilder AddLastScore(int lastScore);

        IPlayerBuilder AddUnlockedColors(BallColor color, ISet<BallColor> unlockedColors);

        IPlayerBuilder AddUnlockedRadius(int radius, ISet<int> unlockedRadius);

        IPlayer Build();
    }
}
