﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Albertini_Riccardo.Bonus
{
    public class BonusType
    {
        public static readonly BonusType COIN = new BonusType(15, "coin", true);
        public static readonly BonusType ADDBALL = new BonusType(10, "add_ball", true);
        public static readonly BonusType HORIZONTAL = new BonusType(10, "horizontal_row (1)", false);
        public static readonly BonusType VERTICAL = new BonusType(10, "vertical-row", false);

        public static IEnumerable<BonusType> Values
        {
            get
            {
                yield return COIN;
                yield return ADDBALL;
                yield return HORIZONTAL;
                yield return VERTICAL;
            }
        }

        public int Radius { get; private set; }
        public string Img { get; private set; }
        public bool InstaDelete { get; private set; }

        BonusType(int radius, string img, bool instaDelete)
        {
            this.Radius = radius;
            this.Img = img;
            this.InstaDelete = instaDelete;
        }

        public static BonusType GetBonusByIndex(int index)
        {
            switch (index)
            {
                case 0:
                    return COIN;
                case 1:
                    return ADDBALL;
                case 2:
                    return HORIZONTAL;
                case 3:
                    return VERTICAL;
                default:
                    throw new ArgumentException();
            }
        }
    }
}
