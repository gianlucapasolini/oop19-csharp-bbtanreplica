﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using bbtanReplica_Csharp.Pasolini_Gianluca;
using bbtanReplica_Csharp.Pasolini_Gianluca.Player;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OOP_Test.Pasolini_Gianluca
{
    [TestClass]
    public class PlayersCollectionTest
    {
        IPlayersCollection playersList;

        [TestInitialize]
        public void TestInitialize()
        {
            playersList = new PlayersCollection();
            playersList.AddPlayer("Daniele");
            playersList.AddPlayer("Andrea");
            playersList.AddPlayer("Riccardo");
            playersList.AddPlayer("Gianluca");
            playersList.AddPlayer("Alex");
            playersList.Update();
        }

        [TestMethod]
        public void TestPresentionPlayer()
        {
            Assert.IsFalse(playersList.GetPlayer("Daniele").IsEmpty());
            Assert.IsFalse(playersList.GetPlayer("Andrea").IsEmpty());
            Assert.IsFalse(playersList.GetPlayer("Riccardo").IsEmpty());
            Assert.IsFalse(playersList.GetPlayer("Gianluca").IsEmpty());
            Assert.IsFalse(playersList.GetPlayer("Alex").IsEmpty());
            Assert.IsTrue(playersList.GetPlayer("Gianfranco").IsEmpty());
        }

        [TestMethod]
        public void TestInsertionsBestScore()
        {
            playersList.GetPlayer("Daniele").Get().SetNewScore(50);
            playersList.GetPlayer("Andrea").Get().SetNewScore(40);
            playersList.GetPlayer("Riccardo").Get().SetNewScore(15);
            playersList.GetPlayer("Gianluca").Get().SetNewScore(30);
            playersList.GetPlayer("Alex").Get().SetNewScore(25);
            Assert.AreEqual(playersList.GetPlayer("Daniele").Get().Name, playersList.GetBestPlayer().Get().Name);
        }

        [TestMethod]
        public void TestMultiplePlayerWithSameName()
        {
            Assert.ThrowsException<InvalidOperationException>(() => playersList.AddPlayer("Daniele"));
        }

        [TestMethod]
        public void TestCurrentPlayer()
        {
            playersList.CurrentPlayerName = Optional<string>.Of("Riccardo");
            Assert.AreEqual(playersList.GetPlayer("Riccardo").Get().Name, playersList.CurrentPlayerName.Get());
            playersList.Logout();
            Assert.AreEqual(Optional<string>.Empty().IsPresent, playersList.CurrentPlayerName.IsPresent);
        }

        [TestMethod]
        public void TestUpdatePlayersData()
        {
            Optional<IPlayer> p = playersList.GetPlayer("Alex");
            p.Get().SetNewScore(30);
            playersList.Update();
            playersList = PlayersCollection.GetCollectionFromDisk();
            Assert.AreEqual(30, playersList.GetPlayer("Alex").Get().BestScore);
        }

        [TestMethod]
        public void TestGetPlayersInOrder()
        {
            playersList.Reset();
            playersList.Update();
            IPlayer p1 = new PlayerBuilder().AddNamePlayer("Filippo").AddBestScore(50).Build();
            IPlayer p2 = new PlayerBuilder().AddNamePlayer("Marco").AddBestScore(40).Build();
            IPlayer p3 = new PlayerBuilder().AddNamePlayer("Jack").AddBestScore(80).Build();
            playersList.AddPlayer(p1);
            playersList.AddPlayer(p2);
            playersList.AddPlayer(p3);
            Optional<IList<IPlayer>> orderedList = playersList.GetPlayersInOrderOfBestScore();
            Assert.IsTrue(orderedList.IsPresent);
            IList<IPlayer> manualOrderedList = new List<IPlayer>
            {
                p3,
                p1,
                p2
            };
            CollectionAssert.AreEqual(manualOrderedList.ToList(), orderedList.Get().ToList());
        }
    }
}
