﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bbtanReplica_Csharp.Di_Lillo_Daniele.Serializator
{
    public abstract class AbstractSerializator<X> : ISerializator<X>
    {
        public abstract string ConvertIntoJson(X obj);

        public abstract X ConvertIntoObject(string jsonString);

        public virtual X DeserializeObject(string path, string fileName)
        {
            string jsonString = File.ReadAllText(path + Path.DirectorySeparatorChar + fileName);
            X obj = ConvertIntoObject(jsonString);
            return obj;
        }
        public virtual void Serialize(string path, string fileName, X obj)
        {
            if(File.Exists(path + Path.DirectorySeparatorChar + fileName))
            {
                File.WriteAllText(path + Path.DirectorySeparatorChar + fileName, ConvertIntoJson(obj));
            }
            else
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                FileStream f = File.Create(path + Path.DirectorySeparatorChar + fileName);
                f.Close();
                File.WriteAllText(path + Path.DirectorySeparatorChar + fileName, ConvertIntoJson(obj));
            }
        }
    }
}
