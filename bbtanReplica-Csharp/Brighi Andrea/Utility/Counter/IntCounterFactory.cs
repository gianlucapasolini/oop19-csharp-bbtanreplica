﻿namespace bbtanReplica_Csharp.BrighiAndrea
{
    public class IntCounterFactory : AbstractCounterFactory<int>
    {
        private const int BASIC_STEP = 1;
        private const int BASIC_START = 0;

        private class IntCounter : AbstractCounter
        {
            public IntCounter(int startValue, int step) : base(startValue, step, (value, steps) => value + steps)
            {
            }
        }

        public override ICounter<int> CounterFromValueWithStep(int startValue, int step)
        {
            return new IntCounter(startValue, step);
        }

        public override ICounter<int> CounterWithStep(int step)
        {
            return CounterFromValueWithStep(BASIC_START, step);
        }

        public override ICounter<int> StandardCounter()
        {
            return CounterFromValueWithStep(BASIC_START, BASIC_STEP);
        }

        public override ICounter<int> StandardCounterFromValue(int startValue)
        {
            return CounterFromValueWithStep(startValue, BASIC_STEP);
        }
    }
}
