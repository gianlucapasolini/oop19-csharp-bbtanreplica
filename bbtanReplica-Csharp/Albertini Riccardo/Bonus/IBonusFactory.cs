﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bbtanReplica_Csharp.Pasolini_Gianluca.Element;


namespace bbtanReplica_Csharp.Albertini_Riccardo.Bonus
{
    interface IBonusFactory
    {
        IBonusBlock Random(Point2D pos);
        IBonusBlock Coin(Point2D pos);
        IBonusBlock AddBall(Point2D pos);
        IBonusBlock Horizontal(Point2D pos);
        IBonusBlock Vertical(Point2D pos);
    }
}
