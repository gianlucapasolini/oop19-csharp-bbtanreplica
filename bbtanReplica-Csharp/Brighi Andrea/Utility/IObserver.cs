﻿
namespace bbtanReplica_Csharp.BrighiAndrea
{
    public delegate void IObserver<T>(ISource<T> source, T argument);
}
