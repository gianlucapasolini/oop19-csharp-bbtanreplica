﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Di_Lillo_Daniele
{
    interface IUnlockedItems<T>
    {

        void AddItem(T newElement);

        ISet<T> GetItems();

        bool CheckItem(T item);

        void SetCurrentItem(T item);

    }
}
