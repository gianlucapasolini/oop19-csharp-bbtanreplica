﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace bbtanReplica_Csharp.Albertini_Riccardo
{
    public class BallColor
    {
        public static readonly BallColor RED = new BallColor(0, 165, "Rosso", Color.Red);
        public static readonly BallColor BLUE = new BallColor(1, 315, "Blu", Color.Blue);
        public static readonly BallColor LIGHT_BLUE = new BallColor(2, 345, "Azzurro", Color.LightBlue);
        public static readonly BallColor GREEN = new BallColor(3, 45, "Verde", Color.Green);
        public static readonly BallColor LIGHT_GREEN = new BallColor(4, 80, "Verde chiaro", Color.LightGreen);
        public static readonly BallColor AQUA_MARINE = new BallColor(5, 15, "Verde acqua", Color.SeaGreen);
        public static readonly BallColor DARK_PURPLE = new BallColor(6, 285, "Viola scuro", Color.Purple);
        public static readonly BallColor PURPLE = new BallColor(7, 255, "Viola", Color.MediumPurple);
        public static readonly BallColor VIOLET = new BallColor(8, 220, "Violetto", Color.Violet);
        public static readonly BallColor FUCSIA = new BallColor(9, 195, "Fucsia", Color.Fuchsia);
        public static readonly BallColor YELLOW = new BallColor(10, 105, "Giallo", Color.Yellow);
        public static readonly BallColor ORANGE = new BallColor(11, 135, "Arancione", Color.Orange);
        public static readonly BallColor WHITE = new BallColor(12, 0, "Bianco", Color.White);

        public static IEnumerable<BallColor> Values
        {
            get
            {
                yield return RED;
                yield return BLUE;
                yield return LIGHT_BLUE;
                yield return GREEN;
                yield return LIGHT_GREEN;
                yield return AQUA_MARINE;
                yield return DARK_PURPLE;
                yield return PURPLE;
                yield return VIOLET;
                yield return FUCSIA;
                yield return YELLOW;
                yield return ORANGE;
                yield return WHITE;
            }
        }

        public int Index { get; private set; }
        public int Degree { get; private set; }
        public string Name { get; private set; }
        public Color Color { get; private set; }

        BallColor(int index, int degree, string name, Color color)
        {
            this.Index = index;
            this.Degree = degree;
            this.Name = name;
            this.Color = color;
        }

        public static BallColor GetColorByIndex(int index)
        {
            switch (index)
            {
                case 0:
                    return RED;
                case 1:
                    return BLUE;
                case 2:
                    return LIGHT_BLUE;
                case 3:
                    return GREEN;
                case 4:
                    return LIGHT_GREEN;
                case 5:
                    return AQUA_MARINE;
                case 6:
                    return DARK_PURPLE;
                case 7:
                    return PURPLE;
                case 8:
                    return VIOLET;
                case 9:
                    return FUCSIA;
                case 10:
                    return YELLOW;
                case 11:
                    return ORANGE;
                case 12:
                    return WHITE;
                default:
                    throw new ArgumentException();
            }
        }
    }

 };
