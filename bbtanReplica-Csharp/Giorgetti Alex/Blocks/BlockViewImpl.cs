﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Giorgetti_Alex.Blocks
{
    /// <summary>
    /// This class implements BlockView and allow
    /// creation of View Block
    /// </summary>
    public class BlockViewImpl : BlockView
    {
        /// <summary>
        /// All variables
        /// </summary>
        /// 
        private static int borderWidht = 5;

        private const int RED_LIMIT = 10;
        private const int GREEN_LIMIT = 20;
        private const int YELLOW_LIMIT = 30;

        private const int STANDARD_X = 15;
        private const int DIFFERENT_X = 20;
        private const int STANDARD_Y = 15;

        private Rectangle thisBlock;

        public Rectangle ThisBlock    // the Name property
        {
            get { return this.thisBlock; }
            set { this.thisBlock = value; }
        }

        public BlockImpl LogicBlock    // the Name property
        { get; set; }

        public Pen BorderColor    // the Name property
        { get; set; } = null;

        /// <summary>
        /// The class constructor
        /// </summary>
        /// <param name="x">X position of Block</param>
        /// <param name="y">Y position of Block</param>
        /// <param name="size">Size of Block</param>
        /// <param name="amount">Life amount of Block</param>
        public BlockViewImpl(int x, int y, int size, int amount)
        {
            this.LogicBlock = new BlockImpl(amount, size);
            thisBlock = new Rectangle(x + borderWidht, y + borderWidht, this.LogicBlock.BlockSize, this.LogicBlock.BlockSize);
            this.BorderColor = new Pen(Color.Transparent, borderWidht);
            setColor();
        }

        /// <summary>
        /// This method set the Border color and span (like size) of border
        /// </summary>
        /// <param name="span">Span is just like the size of border</param>
        private void setColor()
        {
            if (this.LogicBlock.ValueBlock <= RED_LIMIT && this.BorderColor.Color != Color.Red)
                this.BorderColor = new Pen(Color.Red, borderWidht);
            else if (this.LogicBlock.ValueBlock <= GREEN_LIMIT && this.BorderColor.Color != Color.Green)
                this.BorderColor = new Pen(Color.Green, borderWidht);
            else if (this.LogicBlock.ValueBlock <= YELLOW_LIMIT && this.BorderColor.Color != Color.Yellow)
                this.BorderColor = new Pen(Color.Yellow, borderWidht);
        }

        /// <summary>
        /// This method set the coordination
        /// X and Y of block Text
        /// Return Point (X, Y)
        /// </summary>
        public Point setTextCoordination()
        {
            int textX, textY;

            if (this.LogicBlock.ValueBlock >= 10)
            {
                textX = this.ThisBlock.X + STANDARD_X;
            }
            else
            {
                textX = this.ThisBlock.X + DIFFERENT_X;
            }

            textY = this.ThisBlock.Y + STANDARD_Y;

            return new Point(textX, textY);
        }

        /// <summary>
        /// This method allow the possibily to change brightness color of block
        /// </summary>
        public void updateBrightness()
        {
            this.BorderColor = new Pen(Color.FromArgb(this.BorderColor.Color.A,
                (int)(this.BorderColor.Color.R * 0.85), (int)(this.BorderColor.Color.G * 0.85), (int)(this.BorderColor.Color.B * 0.85)), borderWidht);
        }

        public void isHitted(int value)
        {
            this.LogicBlock.ValueBlock = this.LogicBlock.ValueBlock - value;
            this.setColor();
            this.updateBrightness();
        }

        public bool deletedBlock()
        {
            return this.LogicBlock.ValueBlock <= 0;
        }
    }
}
