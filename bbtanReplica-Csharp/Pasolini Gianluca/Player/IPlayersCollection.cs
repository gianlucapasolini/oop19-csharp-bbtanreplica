﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Pasolini_Gianluca.Player
{
    public interface IPlayersCollection
    {
        IList<IPlayer> Players { get; set; }
        Optional<string> CurrentPlayerName { get; set; }

        Optional<IPlayer> GetBestPlayer();

        Optional<IPlayer> GetPlayer(string name);

        void AddPlayer(IPlayer player);

        void AddPlayer(string name);

        bool ContainsPlayer(string name);

        void Update();

        Optional<IList<IPlayer>> GetPlayersInOrderOfBestScore();

        void Logout();

        void Reset();
    }
}
