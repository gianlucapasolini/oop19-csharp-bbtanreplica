﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Pasolini_Gianluca.Element
{
    //Class that represent the point2D.
    public class Point2D : IPoint2D, IEquatable<Point2D>
    {
        public double X { get; private set; }
        public double Y { get; private set; }

        //Constructor.
        public Point2D(double x, double y)
        {
            X = x;
            Y = y;
        }

        public static Point2D operator +(Point2D a, Vector2D v) 
            => new Point2D(a.X + v.XComponent, a.Y + v.YComponent);

        public static Vector2D operator -(Point2D a, IPoint2D b) 
            => new Vector2D(a.X - b.X, a.Y - b.Y);

        public double Distance(IPoint2D p)
        {
            return (this - (p)).GetModulus();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Point2D);
        }

        public bool Equals(Point2D other)
        {
            return other != null &&
                   X == other.X &&
                   Y == other.Y;
        }

        public override int GetHashCode()
        {
            int hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }
    }
}
