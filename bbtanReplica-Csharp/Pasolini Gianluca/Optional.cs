﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Pasolini_Gianluca
{
    public class Optional<T>
    {
        private T value;
        public bool IsPresent { get; private set; } = false;

        private Optional() { }

        public static Optional<T> Empty()
        {
            return new Optional<T>();
        }

        public static Optional<T> Of(T value)
        {
            if (value != null)
            {
                Optional<T> obj = new Optional<T>();
                obj.Set(value);
                return obj;
            }
            else
                throw new ArgumentException();
        }

        public static Optional<T> OfNullable(T value)
        {
            if (value != null)
            {
                Optional<T> obj = new Optional<T>();
                obj.Set(value);
                return obj;
            }
            return Empty();
        }

        private void Set(T value)
        {
            this.value = value;
            IsPresent = true;
        }

        public T Get()
        {
            return value;
        }

        public bool IsEmpty()
        {
            return IsPresent == false;
        }
    }
}
