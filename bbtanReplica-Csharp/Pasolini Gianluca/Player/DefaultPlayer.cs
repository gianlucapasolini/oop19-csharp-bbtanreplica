﻿using bbtanReplica_Csharp.Albertini_Riccardo;
using bbtanReplica_Csharp.Di_Lillo_Daniele;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Pasolini_Gianluca.Player
{
    public class DefaultPlayer : IPlayer
    {
        private UnlockedItems<BallColor> colors;
        private UnlockedItems<int> radius;
        public int Money { get; private set; }
        public int BestScore { get; private set; }
        public int LastScore { get; private set; }
        private string name;
        public string Name { get { return name; } set { name = value.ToUpper(); } }

        public DefaultPlayer()
        {
            colors = new UnlockedItems<BallColor>(BallColor.WHITE);
            radius = new UnlockedItems<int>(8);
            Money = 0;
            BestScore = 0;
            LastScore = 0;
            Name = "NO_NAME";
        }

        public ISet<BallColor> GetColors() => colors.GetItems();

        public BallColor GetCurrentColor() => colors.CurrentItem;

        public void AddNewColor(BallColor newColor) => colors.AddItem(newColor);

        public void SetCurrentColor(BallColor color) => colors.SetCurrentItem(color);

        public int GetCurrentBallRadius() => radius.CurrentItem;

        public ISet<int> GetUnlockedRadius() => radius.GetItems();

        public void AddUnlockedRadius(int radius) => this.radius.AddItem(radius);

        public void ModifyCurrentBallRadius(int radius) => this.radius.SetCurrentItem(radius);

        public bool HasEnoughMoney(int price) => Money >= price;

        public void AddMoney(int money) => Money += money;

        public void UseMoney(int money) => Money -= money;

        public void SetNewScore(int newScore)
        {
            if (BestScore < newScore)
            {
                BestScore = newScore;
            }
            LastScore = newScore;
        }

        public void SetRadiusList(int radius, ISet<int> unlockedRadius)
        {
            this.radius = new UnlockedItems<int>(radius, unlockedRadius);
        }

        public void SetColorsList(BallColor color, ISet<BallColor> unlockedColors)
        {
            colors = new UnlockedItems<BallColor>(color, unlockedColors);
        }
    }
}
