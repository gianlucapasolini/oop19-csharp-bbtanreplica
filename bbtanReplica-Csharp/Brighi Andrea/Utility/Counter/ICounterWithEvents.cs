﻿using System;

namespace bbtanReplica_Csharp.BrighiAndrea
{
    public interface ICounterWithEvents<T> : ICounter<T>, ISource<T> where T : IComparable
    {
    }
}