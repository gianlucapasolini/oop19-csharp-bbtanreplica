﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bbtanReplica_Csharp.Pasolini_Gianluca.Element;


namespace bbtanReplica_Csharp.Albertini_Riccardo.Bonus
{
    public interface IBonusBlock
    {
        Point2D Pos { get; set; }
        bool Hitted { get;  }
        double GetRadius();
        bool IsInstatDelete();
        string GetImg();
        bool Hit();

    }
}
