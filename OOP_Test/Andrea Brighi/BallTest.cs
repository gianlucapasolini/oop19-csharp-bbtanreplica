﻿using System;
using System.Linq;
using bbtanReplica_Csharp.BrighiAndrea.Ball;
using bbtanReplica_Csharp.Pasolini_Gianluca.Element;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OOP_Test.Andrea_Brighi
{
    [TestClass]
    public class BallTest
    {

        [TestMethod]
        public void TestBuilder()
        {

            int invalidNUmber = -2;
            IBallBuilder builder = new BallBuilder();

            Assert.ThrowsException<ArgumentException>(() => builder.Build());

            Assert.ThrowsException<ArgumentException>(() => builder.AddRadius(invalidNUmber));

            Assert.ThrowsException<ArgumentException>(() => builder.AddDamage(invalidNUmber));

            Assert.ThrowsException<ArgumentException>(() => builder.AddMovement(null));

            Assert.ThrowsException<ArgumentException>(() => builder.AddStop(null));

            Assert.ThrowsException<ArgumentException>(() => builder.AddStartPosition(null));
        }


        [TestMethod]
        public void TestBallMovementByTime()
        {

            int limit = 4;
            IBallBuilder builder = new BallBuilder();
            Vector2D direction = new Vector2D(1, 1);
            Point2D position = new Point2D(0, 0);

            IBall ball = builder.AddStartPosition(position).Build();
            ball.Direction = direction;

            long timeInterval = 20;
            foreach (int ignored in Enumerable.Range(0, limit))
            {
                ball.MoveByTime(timeInterval);
                Assert.AreEqual(position + ball.Direction * (timeInterval * ball.Speed), ball.Position);
                position = ball.Position;
            }
        }

        [TestMethod]
        public void TestBallMovementByDistance()
        {

            int limit = 6;
            IBallBuilder builder = new BallBuilder();

            Vector2D direction = new Vector2D(1, 1);
            Point2D position = new Point2D(0, 0);

            IBall ball = builder.AddStartPosition(position)
                    .AddMovement(i =>Assert.IsFalse(i.IsStationary))
                    .AddStop(i =>Assert.IsTrue(i.IsStationary))
                    .Build();
            ball.Direction = direction;

            double distance = 40;
            foreach (int ignored in Enumerable.Range(0, limit))
            {
                ball.MoveByDistance(distance);
                Assert.AreEqual((position + ball.Direction * distance), ball.Position);
                position = ball.Position;
            }
            ball.Direction = new Vector2D(0, 0);
        }

        [TestMethod]
        public void TestBallCollision()
        {
            Vector2D direction = new Vector2D(1, 0);
            Vector2D newDirection = new Vector2D(0, 0);
            Point2D position = new Point2D(0, 0);
            IBallBuilder builder = new BallBuilder();

            IBall ball = builder.AddStartPosition(position).Build();
            ball.Direction = direction;
            ball.MoveByDistance(2);

            ball.Collision(position + direction, newDirection);

            Assert.AreEqual(position + direction + newDirection, ball.Position);

            Assert.ThrowsException<ArgumentException>(() => ball.Collision(position, newDirection));
        }
    }
}
