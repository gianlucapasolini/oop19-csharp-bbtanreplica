﻿using bbtanReplica_Csharp.Pasolini_Gianluca.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bbtanReplica_Csharp.Di_Lillo_Daniele.Serializator
{
    public class SerializatorDecorator : AbstractSerializator<IPlayersCollection>
    {
        protected PlayersCollectionSerializator Serializator { get; private set; }

        protected SerializatorDecorator(PlayersCollectionSerializator _serializator)
        {
            Serializator = _serializator;
        }

        public override string ConvertIntoJson(IPlayersCollection obj)
        {
            return Serializator.ConvertIntoJson(obj);
        }

        public override IPlayersCollection ConvertIntoObject(string jsonString)
        {
            return Serializator.ConvertIntoObject(jsonString);
        }
    }
}
