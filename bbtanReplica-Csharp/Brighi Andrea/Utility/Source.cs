﻿using System.Collections.Generic;
using System.Linq;

namespace bbtanReplica_Csharp.BrighiAndrea
{
    public class Source<T> : ISource<T>
    {

        private readonly ISet<IObserver<T>> observers = new HashSet<IObserver<T>>();

        public void AddObserver(IObserver<T> observer)
        {
            observers.Add(observer);
        }

        public ISet<IObserver<T>> GetObserversSet()
        {
            return observers.ToHashSet();
        }

        public void NotifyObservers(T arg)
        {
            foreach (IObserver<T> observer in observers)
            {
                observer(this, arg);
            }
                
        }

        public void RemoveObserver(IObserver<T> observer)
        {
            observers.Remove(observer);
        }
    }
}
