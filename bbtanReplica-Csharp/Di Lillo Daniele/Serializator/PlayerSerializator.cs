﻿using bbtanReplica_Csharp.Pasolini_Gianluca.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Windows.Forms;
using bbtanReplica_Csharp.Albertini_Riccardo;
using System.Security.Cryptography;

namespace bbtanReplica_Csharp.Di_Lillo_Daniele.Serializator
{
    public class PlayerSerializator : AbstractSerializator<IPlayer>
    {
        public override string ConvertIntoJson(IPlayer obj)
        {            
            return JsonSerializer.Serialize(ConvertObject(obj));
        }

        public object ConvertObject(IPlayer obj)
        {
            var serObj = new
            {
                obj.Name,
                obj.BestScore,
                obj.LastScore,
                obj.Money,
                CurrentBallRadius = obj.GetCurrentBallRadius(),
                UnlockedRadius = obj.GetUnlockedRadius(),
                UnlockedColors = (from p in obj.GetColors()
                           select p.Index),
                CurrentColor = obj.GetCurrentColor().Index

            };
            return serObj;
        }

        public override IPlayer ConvertIntoObject(string jsonString)
        {
            using (JsonDocument document = JsonDocument.Parse(jsonString))
            {
                JsonElement root = document.RootElement;
                string name = root.GetProperty("Name").GetString();
                int bestScore = root.GetProperty("BestScore").GetInt32();
                int lastScore = root.GetProperty("LastScore").GetInt32();
                int money = root.GetProperty("Money").GetInt32();
                BallColor currentColor = BallColor.GetColorByIndex(root.GetProperty("CurrentColor").GetInt32());

                JsonElement unlockedRadius = root.GetProperty("UnlockedRadius");
                ISet<Int32> radiusSet = SerializationMethods.DeserializeSimpleArray(unlockedRadius);
                JsonElement unlockedColors = root.GetProperty("UnlockedColors");
                ISet<Int32> colorsIndexSet = SerializationMethods.DeserializeSimpleArray(unlockedColors);
                ISet<BallColor> colorSet = (from color in colorsIndexSet
                                            select BallColor.GetColorByIndex(color)).ToHashSet();
                IPlayer p = new PlayerBuilder().AddNamePlayer(name)
                                                .AddBestScore(bestScore)
                                                .AddLastScore(lastScore)
                                                .AddInitialMoney(money)
                                                .AddUnlockedColors(currentColor, colorSet)
                                                .Build();
                return p;
            }
        }
    }
}
